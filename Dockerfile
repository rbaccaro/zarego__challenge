FROM python:3.7.2-slim

RUN mkdir -p /app

COPY . ./app

RUN python -m pip install --upgrade pip

RUN pip install -r /app/requirements.txt

CMD ["python3","-u", "/app/app.py"]
