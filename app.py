from pytz import timezone
import datetime
import pymysql
import json
from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
import smtplib
import os

utc_argentina = timezone('America/Argentina/Buenos_Aires')

global conexion
dbServer = 'db'
dbPass = 'zarego123'
dbUser = 'root'  # usurio autorizado para leer la base de datos		
database_name = 'toDoList'
db = pymysql.connect(host=dbServer, user=dbUser, passwd=dbPass, db=database_name, cursorclass=pymysql.cursors.DictCursor,database=database_name,port=3306)
conexion = db

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
sender = os.environ['MAIL_SENDER']
receiver = os.environ['MAIL_RECEIVER']



def SendMail(metodo,title):
	try:
		message = f"""\
		Subject: Se {metodo} la tarea '{title}'
		To: {receiver}
		From: {sender}

		Se {metodo} la tarea {title} recientemente."""

		with smtplib.SMTP("smtp.mailtrap.io", 2525) as server:
			server.login("9383558a2951c6", "98f83325db8d77")
			server.sendmail(sender, receiver, message)
	except Exception as error:
		print(error)


@app.route("/ListTasks", methods=['GET'])
def getListTask():
	try:
		cursor = db.cursor()
		cursor.execute("SELECT * FROM `tasks` where status not in (3)") #status distinto al status de borrado logico que es 3
		tasks = cursor.fetchall()
		cursor.close()
		return jsonify(tasks)
	except Exception as error:
		print(error)
		return jsonify(error="Operación no valida"),500

@app.route("/Task", methods=['POST'])
def addTask():
	try:
		cursor = db.cursor()
		title = request.form["title"]
		description = request.form["description"]
		status = request.form["status"]
		consulta = "INSERT INTO `tasks`(`id`, `title`, `description`,`status`,`created_at`,`updated_at`) VALUES (NULL, %s, %s,%s,%s,NULL);"
		cursor.execute(consulta, (title,description,status,str(datetime.datetime.now(tz=utc_argentina).strftime('%Y-%m-%d %H:%M:%S'))))
		db.commit()
		SendMail("agrego",title)		
		cursor.close()
		return jsonify({"status": "Registro guardado correctamente."}), 200
	except Exception as error:
		print(error)
		return jsonify(error="Operación no valida"), 500

@app.route("/Task", methods=['PUT'])
def updateTask():
	try:
		cursor = db.cursor()
		title = request.form["title"]
		description = request.form["description"]
		status = request.form["status"]
		id = request.form["id"]
		updated_at = str(datetime.datetime.now(tz=utc_argentina).strftime('%Y-%m-%d %H:%M:%S'))
		consulta = "UPDATE `tasks` SET `title`= %s,`description`= %s, `status`= %s,`updated_at`= %s WHERE `id`=%s"
		cursor.execute(consulta, (title, description,status,updated_at, id))
		db.commit()
		if status == 3:
			SendMail('elimino',title)
		else:
			SendMail('modifico',title)
		cursor.close()
		return jsonify({"status": "Registro guardado correctamente."}), 200
	except Exception as error:
		print(error)
		return jsonify(error="Parametros incorrectos"), 500



def main():
	PUERTO_FLASK = os.environ['PUERTO_FLASK']
	app.run(host='0.0.0.0', debug=True, port=PUERTO_FLASK)

if __name__=='__main__':
	main()